﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SoftNS_Player
{
    /// <summary>
    /// Methods for deserialisation of a VoiceBox (Configuration and Audiofile)
    /// </summary>
    class VoiceBoxFileUtils : IDisposable
    {
        public void Dispose(){}

        public bool voiceBoxIsPresent(string folderPath)
        {
            if (fileExtensionIsPresent(folderPath, "wav") && fileExtensionIsPresent(folderPath, "snsx"))
                return true;

            return false;
        }

        private bool fileExtensionIsPresent(string folderPath, string extension)
        {
            Regex rgx_fe = new Regex(@".*\." + extension);

            foreach (String file in System.IO.Directory.GetFiles(folderPath))
            {
                if (rgx_fe.Match(file).Success) return true;

            }

            return false;
        }

        public VoiceBox deserialiseVoiceBox(string folderPath)
        {
            VoiceBox t_vb = new VoiceBox();

            Regex reg = new Regex(@".*\.snsx");
            string serialFile = "";

            foreach (String file in System.IO.Directory.GetFiles(folderPath))
            {
                Match match = reg.Match(file);
                if (match.Success) serialFile = file;
                break;
            }

            if (serialFile.Length.Equals(0))
            {
                Console.WriteLine("Did not find a .snsx file");
            }
            else
            {
                string serialFileFull = serialFile;
                Console.WriteLine("SerialFileFull: {0}", serialFileFull);

                //System.IO.File.Copy(serialFileFull, folderPath + @"\" + System.IO.Path.GetFileNameWithoutExtension(serialFileFull) + ".txt");

                string[] serialFileText = System.IO.File.ReadAllLines(serialFileFull);

                Console.WriteLine("Audiofile: {0}", serialFileText[0].Substring(10, serialFileText[0].Length - 10));
                t_vb.audiofile = serialFileText[0].Substring(10, serialFileText[0].Length - 10);

                Console.WriteLine("Description: {0}", serialFileText[1].Substring(12, serialFileText[1].Length - 12));
                t_vb.audioDescription = serialFileText[1].Substring(12, serialFileText[1].Length - 12);

                Tuple<String, TimeSpan, TimeSpan> rawSection;

                for (int i = 3; i < serialFileText.Length; i++)
                {
                    if (serialFileText[i].Length > 0)
                    {
                        rawSection = lineToSections(serialFileText[i]);
                        t_vb.addSection(rawSection.Item1, rawSection.Item2, rawSection.Item3);
                    }
                }
            }

            return t_vb;
        }

        private static Tuple<String, TimeSpan, TimeSpan> lineToSections(string line)
        {
            string[] tokens = line.Split('\t');

            return new Tuple<String, TimeSpan, TimeSpan>(
                tokens[0],
                stringToTimeSpan(tokens[1]),
                stringToTimeSpan(tokens[2]));
        }

        private static TimeSpan stringToTimeSpan(string str)
        {
            string[] ts_hmsms = str.Split(':');

            string[] ts_sms;

            ts_sms = ts_hmsms[2].Split('.');

            // ts_hmsms[0]  hours
            // ts_hmsms[1]  minutes
            // ts_hmsms[2]  seconds and milliseconds separated by decimal
            // - ts_sms[0]  seconds
            // - ts_sms[1]  milliseconds
            TimeSpan t_TimeSpan = new TimeSpan(0, 0, Int32.Parse(ts_hmsms[1]), Int32.Parse(ts_sms[0]), Int32.Parse(ts_sms.Length.Equals(1) ? "000" : ts_sms[1].Substring(0, 3)));

            Console.WriteLine();

            Console.WriteLine(Int32.Parse(ts_hmsms[0]).ToString());
            Console.WriteLine(Int32.Parse(ts_hmsms[1]).ToString());
            Console.WriteLine(Int32.Parse(ts_sms[0]).ToString());
            Console.WriteLine(Int32.Parse(ts_sms.Length.Equals(1) ? "000" : ts_sms[1].Substring(0, 3)).ToString()); // 700000 -> 700

            Console.WriteLine("\tNew TimeSpan: " + t_TimeSpan.ToString());
            return t_TimeSpan;
        }
    }
}
