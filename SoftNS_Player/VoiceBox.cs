﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace SoftNS_Player
{
    public class VoiceBox
    {
        public string audiofile { get; set; }
        public string audioDescription { get; set; }

        public Dictionary<string, Tuple<TimeSpan, TimeSpan>> sections = new Dictionary<string, Tuple<TimeSpan, TimeSpan>>();

        public void addSection(string ident, TimeSpan start, TimeSpan duration)
        {
            sections.Add(ident, new Tuple<TimeSpan, TimeSpan>(start, duration));
        }

        public Dictionary<string, Tuple<TimeSpan, TimeSpan>> getSections() { return this.sections; }

        public void removeSection(string ident)
        {
            sections.Remove(ident);
        }

        public Tuple<TimeSpan, TimeSpan> getSection(string ident)
        {
            foreach (KeyValuePair<string, Tuple<TimeSpan, TimeSpan>> pair in sections)
                if (pair.Key.Equals(ident))
                    return pair.Value;

            return null;
        }

        public void printVoiceBoxToConsole()
        {
            Console.WriteLine("Audiofile: " + audiofile);
            Console.WriteLine("Description: " + audioDescription);
            foreach (KeyValuePair<string, Tuple<TimeSpan, TimeSpan>> pair in sections)
                Console.WriteLine(pair.Key + " : " + pair.Value.Item1.ToString() + " : " + pair.Value.Item2.ToString());
        }

        public bool isIdent(string ident)
        {
            Debug.WriteLine("Voicebox.isIdent(): " + ident);

            foreach (KeyValuePair<string, Tuple<TimeSpan, TimeSpan>> pair in sections)
                if (pair.Key.Equals(ident)) return true;

            return false;
        }
    }
}
