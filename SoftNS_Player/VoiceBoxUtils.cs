﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NAudio.Wave;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows.Forms;

namespace SoftNS_Player
{
    /// <summary>
    /// Methods of applying VoiceBox objects
    /// </summary>
    public class VoiceBoxUtils : IDisposable
    {
        public void Dispose(){}

        public VoiceBoxUtils() { }

        public void playSection(string directory, VoiceBox t_vb, string ident, int gracePeriod)
        {
            Debug.WriteLine("!!! PLAYBACK !!!\n\tPLAYING IDENT");
            string soundFile = directory + @"\" + t_vb.audiofile;
            //string soundFile = @"C:\Users\Harley\Documents\temp\NATO_VOICEBOX\NATO_Phonetic_Alphabet_reading.wav";

            using (WaveFileReader wfr = new WaveFileReader(soundFile))
            using (WaveChannel32 wc = new WaveChannel32(wfr) { PadWithZeroes = false })
            using (DirectSoundOut audioOutput = new DirectSoundOut())
            {
                Tuple<TimeSpan, TimeSpan> sectionTimeSpans = t_vb.getSection(ident);

                audioOutput.Init(wc);
                wfr.CurrentTime = wfr.CurrentTime.Add(sectionTimeSpans.Item1);
                audioOutput.Play();
                Thread.Sleep(sectionTimeSpans.Item2);
                audioOutput.Stop();
                Thread.Sleep(gracePeriod);
            }
        }

        public void playSectionFixedPeriod(string directory, VoiceBox t_vb, string ident, int pausePeriod)
        {
            Debug.WriteLine("!!! PLAYBACK !!!\n\tPLAYING IDENT FIXED PERIOD");
            string soundFile = directory + @"\" + t_vb.audiofile;
            //string soundFile = @"C:\Users\Harley\Documents\temp\NATO_VOICEBOX\NATO_Phonetic_Alphabet_reading.wav";

            using (WaveFileReader wfr = new WaveFileReader(soundFile))
            using (WaveChannel32 wc = new WaveChannel32(wfr) { PadWithZeroes = false })
            using (DirectSoundOut audioOutput = new DirectSoundOut())
            {
                Tuple<TimeSpan, TimeSpan> sectionTimeSpans = t_vb.getSection(ident);

                audioOutput.Init(wc);
                wfr.CurrentTime = wfr.CurrentTime.Add(sectionTimeSpans.Item1);
                audioOutput.Play();
                Debug.WriteLine("\bFixed Period - VoiceBox pause:\t" + sectionTimeSpans.Item2.Milliseconds);
                Thread.Sleep(sectionTimeSpans.Item2); 
                audioOutput.Stop();
                Debug.WriteLine("\bFixed Period - PausePeriod pause:\t" + (sectionTimeSpans.Item2.Milliseconds > pausePeriod ? 0 : pausePeriod - sectionTimeSpans.Item2.Milliseconds));
                Thread.Sleep(sectionTimeSpans.Item2.Milliseconds > pausePeriod ? 0 : pausePeriod - sectionTimeSpans.Item2.Milliseconds);
            }
        }

        internal bool checkSequence(VoiceBox t_vb, string[] lines)
        {
            Regex rgx_comment = new Regex(@"\/{2}.*");
            Regex rgx_inlinePeriod = new Regex(@"_P");
            Regex rgx_isNumber = new Regex(@"^\d+$");

            foreach (string t_str in lines)
            {
                string str = t_str.Trim();

                if (rgx_comment.Match(str).Success)
                    Debug.WriteLine("C:\t" + str);
                else if (str.Length.Equals(0))
                    Debug.WriteLine("S:\t" + str);
                else if (rgx_inlinePeriod.Match(str).Success)
                    Debug.WriteLine("S:\t" + str);
                else
                {
                    string[] tokens = str.Split(new Char[] { '\t', ' ' });

                    foreach (string token in tokens)
                        if (!t_vb.isIdent(token) && !rgx_isNumber.Match(token).Success)
                        {
                            Debug.WriteLine("!!! VoiceBox.isIdent() returning FALSE on: " + token);
                            return false;
                        }
                }
            }

            return true;
        }

        internal void playSequence(VoiceBox t_vb, string directory, int gracePeriod, string[] lines, bool bFixedPeriod = false, int iFixedPeriod = 100)
        {
            Regex rgx_comment = new Regex(@"\/{2}.*");
            Regex rgx_inlinePeriod = new Regex(@"_P");
            Regex rgx_isNumber = new Regex(@"^\d+$");

            foreach (string t_str in lines)
            {
                string str = t_str.Trim();

                if (rgx_comment.Match(str).Success || str.Length.Equals(0))
                    Debug.WriteLine("playSequence:\n\tComment or Empty line encounter, skipping");
                else
                {
                    string[] tokens = str.Split(new Char[] { '\t', ' ' });

                    foreach (string token in tokens)
                    {
                        if (rgx_inlinePeriod.Match(token).Success)
                            Thread.Sleep(Int32.Parse(token.Substring(2, token.Length - 2)));
                        else
                        {
                            if (rgx_isNumber.Match(token).Success)
                            {
                                string[] numbers = toStringArray(token);

                                foreach (string number in numbers)
                                    if (bFixedPeriod)
                                        this.playSectionFixedPeriod(directory, t_vb, number, iFixedPeriod);
                                    else
                                        this.playSection(directory, t_vb, number, gracePeriod);
                            }
                            else
                            {
                                if (bFixedPeriod)
                                    this.playSectionFixedPeriod(directory, t_vb, token, iFixedPeriod);
                                else
                                    this.playSection(directory, t_vb, token, gracePeriod);
                            }
                        }
                    }
          
                }
            }
        }

        private string[] toStringArray(string s)
        {
            string[] r = new string[s.Length];
            char[] c = s.ToCharArray();

            for (int i = 0; i < s.Length; i++) r[i] = c[i].ToString();
            return r;
        }
    }
}
