﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SoftNS_Player.Forms;

namespace SoftNS_Player
{
    public partial class SoftNS_Player_MainForm : Form
    {
        private static VoiceBox g_ACTIVECVBOBJ;
        private static int      g_PERIOD            =   100;

        public SoftNS_Player_MainForm()
        {
            InitializeComponent();
        }

        private void SoftNS_Player_MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button_VoiceBoxDirectorySelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult result = fbd.ShowDialog();

            if (result.Equals(DialogResult.OK))
            {
                using (VoiceBoxFileUtils vbfu = new VoiceBoxFileUtils())
                {
                    if (vbfu.voiceBoxIsPresent(fbd.SelectedPath))
                    {
                        g_ACTIVECVBOBJ = vbfu.deserialiseVoiceBox(fbd.SelectedPath);

                        textBox_CurrentWorkingDirectory.Text = fbd.SelectedPath;

                        textBox_LoadedVBAudiofile.Text = g_ACTIVECVBOBJ.audiofile;
                        label_playerIdentsLoadedAudiofile.Text = g_ACTIVECVBOBJ.audiofile;

                        textBox_LoadedVBDescription.Text = g_ACTIVECVBOBJ.audioDescription;

                        populateListBox_VoiceBoxIdentsList(g_ACTIVECVBOBJ);
                    }
                    else
                        MessageBox.Show("A Valid VoiceBox configuration (.snsx) and audiofile (.wav) were not found in the specified folder.");
                }
            }
        }

        private void populateListBox_VoiceBoxIdentsList(VoiceBox t_vb)
        {
            foreach (KeyValuePair<string, Tuple<TimeSpan, TimeSpan>> pair in t_vb.getSections())
            {
                listBox_VoiceBoxIdentsList.Items.Add(pair.Key);
            }
        }

        private void button_playListBoxIdent_Click(object sender, EventArgs e)
        {
            if (listBox_VoiceBoxIdentsList.SelectedItem != null)
                using (VoiceBoxUtils vbu = new VoiceBoxUtils())
                    vbu.playSection(textBox_CurrentWorkingDirectory.Text, g_ACTIVECVBOBJ, listBox_VoiceBoxIdentsList.SelectedItem.ToString(), 100);
        }

        private void button_playSequence_Click(object sender, EventArgs e)
        {
            if (listBox_VoiceBoxIdentsList.Items.Count.Equals(0))
                MessageBox.Show("No VoiceBox is loaded or there are no idents defined.");
            else
            {
                label_playbackStatus.Text = "PLAYING";
                label_playbackStatus.ForeColor = Color.Green;
                this.Refresh();

                using (VoiceBoxUtils vbu = new VoiceBoxUtils())
                    if (vbu.checkSequence(g_ACTIVECVBOBJ, richTextBox_sequencer.Lines))
                        vbu.playSequence(
                            g_ACTIVECVBOBJ,
                            textBox_CurrentWorkingDirectory.Text,
                            g_PERIOD, richTextBox_sequencer.Lines,
                            checkBox_FixedPeriod.Checked,
                            (int)numeric_fixedPeriod.Value);
                    else
                        MessageBox.Show("A non existant ident or bad foramtting was encountered.");

                label_playbackStatus.Text = "PAUSED";
                label_playbackStatus.ForeColor = Color.Red;
            }
        }

        private void button_checkSequence_Click(object sender, EventArgs e)
        {
            using (VoiceBoxUtils vbu = new VoiceBoxUtils())
                if (vbu.checkSequence(g_ACTIVECVBOBJ, richTextBox_sequencer.Lines))
                    MessageBox.Show("The sequence passes the ident and formatting check.");
                else
                    MessageBox.Show("A non existant ident or bad foramtting was encountered.");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (SequencerHelp f = new SequencerHelp()) f.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AboutForm a = new AboutForm()) a.ShowDialog();
        }

        private void listBox_VoiceBoxIdentsList_DoubleClick(object sender, EventArgs e)
        {
            if (listBox_VoiceBoxIdentsList.SelectedItem != null)
                using (VoiceBoxUtils vbu = new VoiceBoxUtils())
                    vbu.playSection(textBox_CurrentWorkingDirectory.Text, g_ACTIVECVBOBJ, listBox_VoiceBoxIdentsList.SelectedItem.ToString(), 100);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://naudio.codeplex.com/license");
        }

    }
}
