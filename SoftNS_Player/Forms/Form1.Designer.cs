﻿namespace SoftNS_Player
{
    partial class SoftNS_Player_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SoftNS_Player_MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel_Version = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sequencerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.button_VoiceBoxDirectorySelect = new System.Windows.Forms.Button();
            this.tabControl_MainForm = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.linkLabel_naudio = new System.Windows.Forms.LinkLabel();
            this.label_naudio = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_LoadedVBDescription = new System.Windows.Forms.TextBox();
            this.textBox_LoadedVBAudiofile = new System.Windows.Forms.TextBox();
            this.textBox_CurrentWorkingDirectory = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox_FixedPeriod = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.numeric_fixedPeriod = new System.Windows.Forms.NumericUpDown();
            this.label_playbackStatus = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.button_checkSequence = new System.Windows.Forms.Button();
            this.button_playSequence = new System.Windows.Forms.Button();
            this.richTextBox_sequencer = new System.Windows.Forms.RichTextBox();
            this.button_playListBoxIdent = new System.Windows.Forms.Button();
            this.label_playerIdentsLoadedAudiofile = new System.Windows.Forms.Label();
            this.label_playbackSettings = new System.Windows.Forms.Label();
            this.label_sequencer = new System.Windows.Forms.Label();
            this.label_availableIdents = new System.Windows.Forms.Label();
            this.listBox_VoiceBoxIdentsList = new System.Windows.Forms.ListBox();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl_MainForm.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_fixedPeriod)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_Version});
            this.statusStrip1.Location = new System.Drawing.Point(0, 577);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(524, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel_Version
            // 
            this.toolStripStatusLabel_Version.Name = "toolStripStatusLabel_Version";
            this.toolStripStatusLabel_Version.Size = new System.Drawing.Size(41, 17);
            this.toolStripStatusLabel_Version.Text = "V 0.0.1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.sequencerToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(524, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // sequencerToolStripMenuItem
            // 
            this.sequencerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTextFileToolStripMenuItem,
            this.loadTextFileToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.sequencerToolStripMenuItem.Name = "sequencerToolStripMenuItem";
            this.sequencerToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.sequencerToolStripMenuItem.Text = "Sequencer";
            // 
            // saveTextFileToolStripMenuItem
            // 
            this.saveTextFileToolStripMenuItem.Name = "saveTextFileToolStripMenuItem";
            this.saveTextFileToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.saveTextFileToolStripMenuItem.Text = "Save text file ...";
            // 
            // loadTextFileToolStripMenuItem
            // 
            this.loadTextFileToolStripMenuItem.Name = "loadTextFileToolStripMenuItem";
            this.loadTextFileToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.loadTextFileToolStripMenuItem.Text = "Load text file ...";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "VoiceBox Selection";
            // 
            // button_VoiceBoxDirectorySelect
            // 
            this.button_VoiceBoxDirectorySelect.Location = new System.Drawing.Point(160, 18);
            this.button_VoiceBoxDirectorySelect.Name = "button_VoiceBoxDirectorySelect";
            this.button_VoiceBoxDirectorySelect.Size = new System.Drawing.Size(75, 20);
            this.button_VoiceBoxDirectorySelect.TabIndex = 4;
            this.button_VoiceBoxDirectorySelect.Text = "Select...";
            this.button_VoiceBoxDirectorySelect.UseVisualStyleBackColor = true;
            this.button_VoiceBoxDirectorySelect.Click += new System.EventHandler(this.button_VoiceBoxDirectorySelect_Click);
            // 
            // tabControl_MainForm
            // 
            this.tabControl_MainForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl_MainForm.Controls.Add(this.tabPage1);
            this.tabControl_MainForm.Controls.Add(this.tabPage2);
            this.tabControl_MainForm.Location = new System.Drawing.Point(0, 27);
            this.tabControl_MainForm.Name = "tabControl_MainForm";
            this.tabControl_MainForm.SelectedIndex = 0;
            this.tabControl_MainForm.Size = new System.Drawing.Size(524, 547);
            this.tabControl_MainForm.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Menu;
            this.tabPage1.Controls.Add(this.linkLabel_naudio);
            this.tabPage1.Controls.Add(this.label_naudio);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.textBox_LoadedVBDescription);
            this.tabPage1.Controls.Add(this.textBox_LoadedVBAudiofile);
            this.tabPage1.Controls.Add(this.textBox_CurrentWorkingDirectory);
            this.tabPage1.Controls.Add(this.button_VoiceBoxDirectorySelect);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(516, 521);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "VoiceBox Configuration";
            // 
            // linkLabel_naudio
            // 
            this.linkLabel_naudio.AutoSize = true;
            this.linkLabel_naudio.Location = new System.Drawing.Point(7, 496);
            this.linkLabel_naudio.Name = "linkLabel_naudio";
            this.linkLabel_naudio.Size = new System.Drawing.Size(182, 13);
            this.linkLabel_naudio.TabIndex = 8;
            this.linkLabel_naudio.TabStop = true;
            this.linkLabel_naudio.Text = "https://naudio.codeplex.com/license";
            this.linkLabel_naudio.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label_naudio
            // 
            this.label_naudio.AutoSize = true;
            this.label_naudio.Location = new System.Drawing.Point(6, 479);
            this.label_naudio.Name = "label_naudio";
            this.label_naudio.Size = new System.Drawing.Size(268, 13);
            this.label_naudio.TabIndex = 7;
            this.label_naudio.Text = "The SoftNS Suite incoorperates the NAudio C# libraries";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Loaded VoiceBox Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Loaded VoiceBox Audiofile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Current working directory";
            // 
            // textBox_LoadedVBDescription
            // 
            this.textBox_LoadedVBDescription.Location = new System.Drawing.Point(12, 150);
            this.textBox_LoadedVBDescription.Name = "textBox_LoadedVBDescription";
            this.textBox_LoadedVBDescription.ReadOnly = true;
            this.textBox_LoadedVBDescription.Size = new System.Drawing.Size(496, 20);
            this.textBox_LoadedVBDescription.TabIndex = 5;
            // 
            // textBox_LoadedVBAudiofile
            // 
            this.textBox_LoadedVBAudiofile.Location = new System.Drawing.Point(11, 111);
            this.textBox_LoadedVBAudiofile.Name = "textBox_LoadedVBAudiofile";
            this.textBox_LoadedVBAudiofile.ReadOnly = true;
            this.textBox_LoadedVBAudiofile.Size = new System.Drawing.Size(496, 20);
            this.textBox_LoadedVBAudiofile.TabIndex = 5;
            // 
            // textBox_CurrentWorkingDirectory
            // 
            this.textBox_CurrentWorkingDirectory.Location = new System.Drawing.Point(12, 72);
            this.textBox_CurrentWorkingDirectory.Name = "textBox_CurrentWorkingDirectory";
            this.textBox_CurrentWorkingDirectory.ReadOnly = true;
            this.textBox_CurrentWorkingDirectory.Size = new System.Drawing.Size(496, 20);
            this.textBox_CurrentWorkingDirectory.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Menu;
            this.tabPage2.Controls.Add(this.checkBox_FixedPeriod);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.numeric_fixedPeriod);
            this.tabPage2.Controls.Add(this.label_playbackStatus);
            this.tabPage2.Controls.Add(this.linkLabel1);
            this.tabPage2.Controls.Add(this.button_checkSequence);
            this.tabPage2.Controls.Add(this.button_playSequence);
            this.tabPage2.Controls.Add(this.richTextBox_sequencer);
            this.tabPage2.Controls.Add(this.button_playListBoxIdent);
            this.tabPage2.Controls.Add(this.label_playerIdentsLoadedAudiofile);
            this.tabPage2.Controls.Add(this.label_playbackSettings);
            this.tabPage2.Controls.Add(this.label_sequencer);
            this.tabPage2.Controls.Add(this.label_availableIdents);
            this.tabPage2.Controls.Add(this.listBox_VoiceBoxIdentsList);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(516, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "VoiceBox Player";
            // 
            // checkBox_FixedPeriod
            // 
            this.checkBox_FixedPeriod.AutoSize = true;
            this.checkBox_FixedPeriod.Checked = true;
            this.checkBox_FixedPeriod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_FixedPeriod.Location = new System.Drawing.Point(10, 232);
            this.checkBox_FixedPeriod.Name = "checkBox_FixedPeriod";
            this.checkBox_FixedPeriod.Size = new System.Drawing.Size(209, 17);
            this.checkBox_FixedPeriod.TabIndex = 11;
            this.checkBox_FixedPeriod.Text = "Use fixed pause period between idents";
            this.checkBox_FixedPeriod.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(308, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(199, 275);
            this.panel1.TabIndex = 10;
            // 
            // numeric_fixedPeriod
            // 
            this.numeric_fixedPeriod.Location = new System.Drawing.Point(226, 231);
            this.numeric_fixedPeriod.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numeric_fixedPeriod.Minimum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numeric_fixedPeriod.Name = "numeric_fixedPeriod";
            this.numeric_fixedPeriod.Size = new System.Drawing.Size(76, 20);
            this.numeric_fixedPeriod.TabIndex = 9;
            this.numeric_fixedPeriod.Value = new decimal(new int[] {
            1200,
            0,
            0,
            0});
            // 
            // label_playbackStatus
            // 
            this.label_playbackStatus.AutoSize = true;
            this.label_playbackStatus.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_playbackStatus.ForeColor = System.Drawing.Color.Red;
            this.label_playbackStatus.Location = new System.Drawing.Point(6, 493);
            this.label_playbackStatus.Name = "label_playbackStatus";
            this.label_playbackStatus.Size = new System.Drawing.Size(63, 19);
            this.label_playbackStatus.TabIndex = 7;
            this.label_playbackStatus.Text = "PAUSED";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(371, 285);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(137, 13);
            this.linkLabel1.TabIndex = 6;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Help on using the seqencer";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // button_checkSequence
            // 
            this.button_checkSequence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_checkSequence.Location = new System.Drawing.Point(244, 492);
            this.button_checkSequence.Name = "button_checkSequence";
            this.button_checkSequence.Size = new System.Drawing.Size(129, 23);
            this.button_checkSequence.TabIndex = 5;
            this.button_checkSequence.Text = "Check Sequence";
            this.button_checkSequence.UseVisualStyleBackColor = true;
            this.button_checkSequence.Click += new System.EventHandler(this.button_checkSequence_Click);
            // 
            // button_playSequence
            // 
            this.button_playSequence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_playSequence.Location = new System.Drawing.Point(379, 492);
            this.button_playSequence.Name = "button_playSequence";
            this.button_playSequence.Size = new System.Drawing.Size(129, 23);
            this.button_playSequence.TabIndex = 5;
            this.button_playSequence.Text = "Play Sequence";
            this.button_playSequence.UseVisualStyleBackColor = true;
            this.button_playSequence.Click += new System.EventHandler(this.button_playSequence_Click);
            // 
            // richTextBox_sequencer
            // 
            this.richTextBox_sequencer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_sequencer.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_sequencer.Location = new System.Drawing.Point(8, 303);
            this.richTextBox_sequencer.Name = "richTextBox_sequencer";
            this.richTextBox_sequencer.Size = new System.Drawing.Size(500, 183);
            this.richTextBox_sequencer.TabIndex = 4;
            this.richTextBox_sequencer.Text = "// Example sequence for IVONA_NATO_US_KENDRA voicebox\nSTANDBY\nMESSAGEFOLLOWS\n\n123" +
                " 123 456 456\n789 789 900 900\n\n//\nBREAK\n_P1500\nOVER\n\n\n\n";
            // 
            // button_playListBoxIdent
            // 
            this.button_playListBoxIdent.Location = new System.Drawing.Point(184, 26);
            this.button_playListBoxIdent.Name = "button_playListBoxIdent";
            this.button_playListBoxIdent.Size = new System.Drawing.Size(118, 23);
            this.button_playListBoxIdent.TabIndex = 3;
            this.button_playListBoxIdent.Text = "Play Selected Ident";
            this.button_playListBoxIdent.UseVisualStyleBackColor = true;
            this.button_playListBoxIdent.Click += new System.EventHandler(this.button_playListBoxIdent_Click);
            // 
            // label_playerIdentsLoadedAudiofile
            // 
            this.label_playerIdentsLoadedAudiofile.AutoSize = true;
            this.label_playerIdentsLoadedAudiofile.Location = new System.Drawing.Point(134, 7);
            this.label_playerIdentsLoadedAudiofile.Name = "label_playerIdentsLoadedAudiofile";
            this.label_playerIdentsLoadedAudiofile.Size = new System.Drawing.Size(104, 13);
            this.label_playerIdentsLoadedAudiofile.TabIndex = 2;
            this.label_playerIdentsLoadedAudiofile.Text = "No VoiceBox loaded";
            // 
            // label_playbackSettings
            // 
            this.label_playbackSettings.AutoSize = true;
            this.label_playbackSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_playbackSettings.Location = new System.Drawing.Point(6, 208);
            this.label_playbackSettings.Name = "label_playbackSettings";
            this.label_playbackSettings.Size = new System.Drawing.Size(135, 20);
            this.label_playbackSettings.TabIndex = 1;
            this.label_playbackSettings.Text = "Playback Settings";
            // 
            // label_sequencer
            // 
            this.label_sequencer.AutoSize = true;
            this.label_sequencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sequencer.Location = new System.Drawing.Point(6, 280);
            this.label_sequencer.Name = "label_sequencer";
            this.label_sequencer.Size = new System.Drawing.Size(87, 20);
            this.label_sequencer.TabIndex = 1;
            this.label_sequencer.Text = "Sequencer";
            // 
            // label_availableIdents
            // 
            this.label_availableIdents.AutoSize = true;
            this.label_availableIdents.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_availableIdents.Location = new System.Drawing.Point(6, 3);
            this.label_availableIdents.Name = "label_availableIdents";
            this.label_availableIdents.Size = new System.Drawing.Size(121, 20);
            this.label_availableIdents.TabIndex = 1;
            this.label_availableIdents.Text = "Available Idents";
            // 
            // listBox_VoiceBoxIdentsList
            // 
            this.listBox_VoiceBoxIdentsList.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_VoiceBoxIdentsList.FormattingEnabled = true;
            this.listBox_VoiceBoxIdentsList.Location = new System.Drawing.Point(10, 26);
            this.listBox_VoiceBoxIdentsList.Name = "listBox_VoiceBoxIdentsList";
            this.listBox_VoiceBoxIdentsList.ScrollAlwaysVisible = true;
            this.listBox_VoiceBoxIdentsList.Size = new System.Drawing.Size(168, 147);
            this.listBox_VoiceBoxIdentsList.TabIndex = 0;
            this.listBox_VoiceBoxIdentsList.DoubleClick += new System.EventHandler(this.listBox_VoiceBoxIdentsList_DoubleClick);
            // 
            // SoftNS_Player_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 599);
            this.Controls.Add(this.tabControl_MainForm);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SoftNS_Player_MainForm";
            this.Text = "SoftNS - VoiceBox Player";
            this.Load += new System.EventHandler(this.SoftNS_Player_MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl_MainForm.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_fixedPeriod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_VoiceBoxDirectorySelect;
        private System.Windows.Forms.TabControl tabControl_MainForm;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_LoadedVBDescription;
        private System.Windows.Forms.TextBox textBox_LoadedVBAudiofile;
        private System.Windows.Forms.TextBox textBox_CurrentWorkingDirectory;
        private System.Windows.Forms.Label label_playerIdentsLoadedAudiofile;
        private System.Windows.Forms.Label label_availableIdents;
        private System.Windows.Forms.ListBox listBox_VoiceBoxIdentsList;
        private System.Windows.Forms.Button button_playListBoxIdent;
        private System.Windows.Forms.RichTextBox richTextBox_sequencer;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button button_playSequence;
        private System.Windows.Forms.Label label_sequencer;
        private System.Windows.Forms.Button button_checkSequence;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Version;
        private System.Windows.Forms.Label label_playbackStatus;
        private System.Windows.Forms.NumericUpDown numeric_fixedPeriod;
        private System.Windows.Forms.Label label_playbackSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox_FixedPeriod;
        private System.Windows.Forms.ToolStripMenuItem sequencerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTextFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadTextFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel_naudio;
        private System.Windows.Forms.Label label_naudio;
    }
}

